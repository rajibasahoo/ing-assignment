package com.ing.mortage.demo;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import com.ing.mortage.demo.model.InterestRate;
import com.ing.mortage.demo.repository.InterestRateRepository;
import com.ing.mortage.demo.services.impl.MortgageServiceImpl;


/**
 * 
 * @author rajiba sahoo
 * This class is the entry point of the application which initializes the list using commandlinerunner.
 * and make all the model,controller,services available for component scanning into class path.
 */


@SpringBootApplication
public class MortgageApplication implements org.springframework.boot.CommandLineRunner{

	InterestRateRepository repository;

	Logger logger = Logger.getLogger(MortgageApplication.class.getName());
	public static void main(String[] args) {
		SpringApplication.run(MortgageApplication.class, args);
	}


	@Bean
	public CommandLineRunner loadData(InterestRateRepository repository) {
		return (args) -> {
			// save a couple of interest rates
			logger.info("Data loading Started...");
			List<InterestRate> irList = Arrays.asList( new InterestRate[] {
							new InterestRate(1,BigDecimal.valueOf(4.505)),
							new InterestRate(2,BigDecimal.valueOf(4.495)),
							new InterestRate(3,BigDecimal.valueOf(4.455)),
							new InterestRate(4,BigDecimal.valueOf(4.415)),
							
						}
					);
			
			repository.saveAll(irList);	
			logger.info("Data loaded Sucessfully...");

		};
	}


	@Override
	public void run(String... args) throws Exception {
		loadData(repository);
		
	}
}


