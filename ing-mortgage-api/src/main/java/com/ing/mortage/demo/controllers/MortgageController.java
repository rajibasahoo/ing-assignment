package com.ing.mortage.demo.controllers;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ing.mortage.demo.model.InterestRate;
import com.ing.mortage.demo.model.Mortgage;
import com.ing.mortage.demo.model.MortgageResponse;
import com.ing.mortage.demo.services.MortgageService;
import com.ing.mortage.demo.services.impl.MortgageServiceImpl;

/**
 * 
 * @author rajiba sahoo
 * This class is the controller which has two end points. based on the type of request correct method will invoke and return the response.
 * 
 */

@RestController
@RequestMapping("/api")
public class MortgageController {
	
	Logger logger = Logger.getLogger(MortgageServiceImpl.class.getName());
	
	@Autowired
	MortgageService mortgageService;
	
	@GetMapping("/interest-rates")
	public ResponseEntity<List<InterestRate>> getInterestRates() {
		logger.info("START getInterestRates API :");
		List<InterestRate> irList = mortgageService.findAllInterestRates();
		if (!irList.isEmpty()) {
			return new ResponseEntity<>(irList, HttpStatus.OK);
		}
		logger.info("END getInterestRates API:");
		return new ResponseEntity<List<InterestRate>>(HttpStatus.NO_CONTENT);
		
	}

	@PostMapping(value = "/mortgage-check")    
	public ResponseEntity<MortgageResponse>   checkMortgage(@RequestBody Mortgage mortgage) {
		logger.info("START checkMortgage API:");
		MortgageResponse mortgageReturn = mortgageService.checkMortgageFeasible(mortgage);
		if (mortgageReturn!=null)
			return new ResponseEntity<>(mortgageReturn, HttpStatus.OK);
		logger.info("END checkMortgage API:");
		return new ResponseEntity<>(mortgageReturn, HttpStatus.BAD_REQUEST);
	}

}
