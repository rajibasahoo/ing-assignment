package com.ing.mortage.demo.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * 
 * @author rajiba sahoo
 * This class is the pojo class which holds parameter related to amount.
 * 
 */

@Embeddable 
public class Amount {
	
	@Column
	private Currency currency;

	@Column(precision=5, scale=2)
	private BigDecimal value;
	
	public Amount(BigDecimal value, Currency currency) {
		this.value = value;
		this.currency = currency;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Amount [currency=" + currency + ", value=" + value + "]";
	}

	
}
