package com.ing.mortage.demo.model;

/**
 * 
 * @author rajiba sahoo
 * This class is the pojo class which holds parameter related to currency.
 * 
 */
public enum Currency {
	
	EUR, USD //based on international codes index - ISO 4217

}
