package com.ing.mortage.demo.model;

import java.math.BigDecimal;

import java.sql.Date;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * 
 * @author rajiba sahoo
 * This class is the pojo class which holds parameter related to interest rate.
 * 
 */

@Entity
public class InterestRate {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;
	
	@Column(name = "maturity_period", nullable = false)
	private Integer maturityPeriod;
	
	@Column(name="interest_rate", nullable=false, precision = 5, scale = 4)
	private BigDecimal intRate;
	
	public InterestRate() {
		super();
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Integer getMaturityPeriod() {
		return maturityPeriod;
	}


	public void setMaturityPeriod(Integer maturityPeriod) {
		this.maturityPeriod = maturityPeriod;
	}


	public BigDecimal getIntRate() {
		return intRate;
	}


	public void setIntRate(BigDecimal intRate) {
		this.intRate = intRate;
	}


	public Date getLastUpdate() {
		return lastUpdate;
	}


	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}


	@Column(name="last_update", nullable=false)
	private Date lastUpdate;
	
	public InterestRate(Integer maturiryPeriod, BigDecimal interestRate) {
		this.maturityPeriod = maturiryPeriod;
		this.intRate = interestRate;
		this.lastUpdate = Date.valueOf(LocalDate.now());
	}

	
	@Override
	public String toString() {
		return "InterestRate [id=" + id + ", maturityPeriod=" + maturityPeriod + ", intRate=" + intRate
				+ ", lastUpdate=" + lastUpdate + "]";
	}


	public BigDecimal getInterestRate() {
		
		return intRate;
	}

}
