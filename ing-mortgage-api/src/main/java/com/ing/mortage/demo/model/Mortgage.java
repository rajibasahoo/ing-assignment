package com.ing.mortage.demo.model;




/**
 * 
 * @author rajiba sahoo
 * This class is the pojo class which holds parameter related to mortgage.
 * 
 */

public class Mortgage {
	
	
	private Amount income;
	
	private Integer maturityPeriod;
	
	private Amount loanValue;
	
	private Amount homeValue;
	
	
	/**
	 * copy mortgage
	 * @param m
	 */
	public Mortgage(Mortgage m){
	
		this.homeValue = m.homeValue;
		this.loanValue = m.loanValue;
		this.maturityPeriod = m.maturityPeriod;
		this.income = m.income;
		
	}

	public Mortgage() {
		
	}

	
	@Override
	public String toString() {
		return "Mortgage [income=" + income + ", maturityPeriod=" + maturityPeriod + ", loanValue=" + loanValue
				+ ", homeValue=" + homeValue + "]";
	}

	public Integer getMaturityPeriod() {
		
		return maturityPeriod;
	}

	public Amount getLoanValue() {
		
		return loanValue;
	}

	

	public Amount getIncome() {
		
		return income;
	}

	

	public Amount getHomeValue() {
		
		return homeValue;
	}

	public void setHomeValue(Amount amount) {
		homeValue=amount;
		
	}

	public void setIncome(Amount amount) {
		income=amount;
		
	}

	public void setLoanValue(Amount amount) {
		loanValue=amount;
		
	}

	public void setMaturityPeriod(int i) {
		maturityPeriod=i;
		
	}

	

}
