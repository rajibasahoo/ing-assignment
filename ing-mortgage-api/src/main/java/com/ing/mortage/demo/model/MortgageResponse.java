package com.ing.mortage.demo.model;

public class MortgageResponse {
	
	private Amount monthlyPayment;
	public Amount getMonthlyPayment() {
		return monthlyPayment;
	}
	public void setMonthlyPayment(Amount monthlyPayment) {
		this.monthlyPayment = monthlyPayment;
	}
	public Boolean getIsFeasible() {
		return isFeasible;
	}
	public void setIsFeasible(Boolean isFeasible) {
		this.isFeasible = isFeasible;
	}
	private Boolean isFeasible;

}
