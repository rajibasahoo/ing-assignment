package com.ing.mortage.demo.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.ing.mortage.demo.model.InterestRate;

/**
 * 
 * @author rajiba sahoo
 * This class is the interface for jparepository which is used in implemented class for finding int rate..
 * 
 */

@Repository
public interface InterestRateRepository extends JpaRepository<InterestRate, Long> {
	
	public Optional<InterestRate> findInterestRateByMaturityPeriod(Integer maturityPeriod);

}
