package com.ing.mortage.demo.services;

import java.util.List;
import java.util.Optional;
import com.ing.mortage.demo.model.InterestRate;
import com.ing.mortage.demo.model.Mortgage;
import com.ing.mortage.demo.model.MortgageResponse;

/**
 * 
 * @author rajiba sahoo
 * This class is the interface for mortgage service which has all the required method like find int rate, check feasible.
 * 
 */
public interface MortgageService {
	
	public List<InterestRate> findAllInterestRates();
	
	public Optional<InterestRate> findInterestRateByMaturityPeriod(Integer maturityPeriod);
	
	public MortgageResponse checkMortgageFeasible(Mortgage mortgage);
	

}
