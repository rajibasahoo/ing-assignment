package com.ing.mortage.demo.services.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ing.mortage.demo.model.Amount;
import com.ing.mortage.demo.model.InterestRate;
import com.ing.mortage.demo.model.Mortgage;
import com.ing.mortage.demo.model.MortgageResponse;
import com.ing.mortage.demo.repository.InterestRateRepository;
import com.ing.mortage.demo.services.MortgageService;

/**
 * 
 * @author rajiba sahoo
 * This class is the main implementation for business logic which will check feasibility based on the condition applied on business logic.
 * 
 */
@Service
public class MortgageServiceImpl implements MortgageService {

	private static final int INCOME_VALUE = 4;
	private static final int MATURITY_PERIOD = 12;
	Logger logger = Logger.getLogger(MortgageServiceImpl.class.getName());
	
	@Autowired
	InterestRateRepository irRepository;	

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<InterestRate> findAllInterestRates() {
		return irRepository.findAll();
	}

	@Override
	public Optional<InterestRate> findInterestRateByMaturityPeriod(Integer maturityPeriod) {
		return irRepository.findInterestRateByMaturityPeriod(maturityPeriod);
	}
	
	/**
	 * the method is calculating the mortgage using the formula and setting feasible or not
	 */
	@Override
	@Transactional
	public MortgageResponse checkMortgageFeasible(Mortgage mortgage) {

		logger.info("START checkMortgageFeasible:");
		//find by the maturity level
		try {
			Optional<InterestRate> optIR = irRepository.findInterestRateByMaturityPeriod(mortgage.getMaturityPeriod());
			MortgageResponse mortReturn = new MortgageResponse();
			
			if (optIR.isPresent()) {
				InterestRate ir = optIR.get();
				Double monthlyRate = calculateMonthlyRate(ir.getInterestRate());
				Double rateFactor = calculateRateFactor(monthlyRate, mortgage.getMaturityPeriod());
				Double monthlyPayment = calculateMonthlyPayment(rateFactor, mortgage.getLoanValue(), monthlyRate);
				logger.info("monthlyrate,ratefactor,monthlypayment calculations are over:");
				BigDecimal mP = BigDecimal.valueOf(monthlyPayment).setScale(2, BigDecimal.ROUND_HALF_DOWN);
				mortReturn.setMonthlyPayment(new Amount(mP, mortgage.getLoanValue().getCurrency()));
				mortReturn.setIsFeasible(true);
				if (monthlyPayment > (mortgage.getIncome().getValue().doubleValue()/INCOME_VALUE) ||monthlyPayment*mortgage.getMaturityPeriod()*MATURITY_PERIOD > mortgage.getHomeValue().getValue().doubleValue()) {
					mortReturn.setIsFeasible(false);
					return mortReturn;			
				}
			}

			logger.info("END checkMortgageFeasible:");
			return mortReturn;
		} catch (Exception e) {
			throw e;
		}
	}
	
	private Double calculateMonthlyPayment(Double rateFactor, Amount principalAmount, Double monthlyRate) {
		Double aux = (monthlyRate*rateFactor)/(rateFactor-1);
		return principalAmount.getValue().doubleValue()*aux;
	}
	
	private Double calculateRateFactor(Double montlhyRate, Integer maturity) {
		return Math.pow((1+montlhyRate), maturity.doubleValue()*MATURITY_PERIOD);
	}
	
	private Double calculateMonthlyRate(BigDecimal anualRate) {
		return (anualRate.doubleValue()/MATURITY_PERIOD)/100;
	}

}
